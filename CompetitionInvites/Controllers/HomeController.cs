﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompetitionInvites.Models;

namespace CompetitionInvites.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //Shall render a new view
        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon"; //Viewbag can be used to share values between controller and view
            return View();
        }
        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult RsvpForm(CompetitionInvite guestResponse) //Using guestResponse to access our CompeitionInvite model
        {
            if (ModelState.IsValid) //If isValid returns as true then
            {
                if (Request.Form["submit"].Equals("Accept Invitation")) //Check if the Accept Invitation button was pressed
                {
                    guestResponse.WillAttend = true; //If it was pressed then we will set WillAttend to be true
                }
                else if (Request.Form["submit"].Equals("Send Regrets")) //Check if the Send Regrets button was checked
                {
                    guestResponse.WillAttend = false; //If so then we set WillAttend to false
                }
                    // TODO: Email response to the party organizer
                    return View("Thanks", guestResponse); //Print out the response to user
            }
            else
            {
                // there is a validation error
                return View();
            }
        }
    }
}