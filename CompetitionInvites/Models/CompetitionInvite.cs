﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CompetitionInvites.Models
{
    public class CompetitionInvite
    {
        //Properties
        public string Address { get; set; }
        public string TwitterAccount { get; set; }

        [Required(ErrorMessage = "Please specify your technical interest")] //Each error message ensures that these fields are filled or the user will get a prompt telling them there is an issue
        public TechnicalInterest Interest { get; set; } //Interest is of type TechnicalInterest which is an enumeration we created below

        [Required(ErrorMessage ="Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        public string Phone { get; set; }

        public bool? WillAttend { get; set; } //Bool is set to nullable bool since user could choose not to fill out field. NOTE this doesnt apply anymore since the dropdown has been replaced with buttons
    }
    public enum TechnicalInterest //Enum containing 4 interests IoT,Cognitive,Wearable,AR_VR
    {
        IoT,Cognitive,Wearable,AR_VR
    }
}